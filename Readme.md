# ITK Pixel RD53 OPC UA Server

## Make sure you have these installed

0. Create your local install path

```
mkdir installed
mkdir installed/include
mkdir installed/$CMTCONFIG/
mkdir installed/$CMTCONFIG/bin
mkdir installed/$CMTCONFIG/lib
```

1. Install python module dependencies

```
alias pip=$LCG_RELEASE_BASE/$TDAQ_LCG_RELEASE/pip/*/$CMTCONFIG/bin/pip
pip install --user colorama
pip install --user pygit2 #this will fail
```

2. Install xsdcxx

```
wget https://www.codesynthesis.com/download/xsd/4.0/linux-gnu/x86_64/xsd-4.0.0-x86_64-linux-gnu.tar.bz2
tar xvf xsd-4.0.0-x86_64-linux-gnu.tar.bz2
cp xsd-4.0.0-x86_64-linux-gnu/bin/xsd installed/$CMTCONFIG/bin/xsdcxx
cp -r xsd-4.0.0-x86_64-linux-gnu/libxsd/xsd installed/include/.
```

3. Install astyle
```
wget https://sourceforge.net/projects/astyle/files/astyle/astyle%203.1/astyle_3.1_linux.tar.gz/download
mv download astyle_3.1_linux.tar.gz
tar xzvf astyle_3.1_linux.tar.gz
cp astyle/build/astyle installed/$CMTCONFIG/bin/astyle
```

## Install quasar for the first time
First installation

1. Assume tdaq-9 is intalled and available

2. Download quasar

```
git clone --recursive https://github.com/quasar-team/quasar -b v1.4.0-rc1
```

4. Create the ITK Pixel RD53A OPC UA server

```
./quasar.py create_project ../opc-ua-itk-rd53a
```

5. Change to new project

```
cd ../opc-ua-itk-rd53a
```

## Configure the build

1. Enable open62541 adapter library for WinCC OA

```
./quasar.py enable_module open62541-compat v1.2.2-rc0
```

2. Copy over the open62541 cmake
 
```
cp ../quasar/open62541_config.cmake .
```

3. Enable build config for open62541

```
./quasar.py set_build_config ./open62541_config.cmake
```

4. Build the server

```
./quasar.py build
```

## Configure custom schema

1. Edit device data

```
vi Design/Design.xml
```

2. Generate the device code

```
./quasar.py generate device
```

3. Build the server

```
./quasar.py build
```
