/* �� Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <thread>

#include "QuasarServer.h"
#include <LogIt.h>
#include <shutdown.h>

#include <DRoot.h>
#include <DQuad.h>
#include <DRD53A.h>

#include "RD53Emulator/Register.h"
#include <queue>
#include <iostream>

QuasarServer::QuasarServer() : BaseQuasarServer()
{
  client = new RD53A::NetioClient();
  client->SetVerbose(false);
}

QuasarServer::~QuasarServer()
{
  delete client;
}

void QuasarServer::mainLoop()
{
    printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

    // Wait for user command to terminate the server thread.

    while(ShutDownFlag() == 0)
    {
      std::this_thread::sleep_for(std::chrono::milliseconds(1000));

      std::cout << "Received register frames: " << std::endl;
      while(client->HasNextRegister(0)){
    	  RD53A::Register reg=client->GetNextRegister(0);
        std::cout << "++ " << reg.ToString() << std::endl;
        //FIXME: Update the DRD53 objects
    	}

    	RD53A::RdReg * rdcmd=new RD53A::RdReg();
    	rdcmd->SetAddress(22);
    	//client->SendCmd(0,rdcmd);
      
    	RD53A::WrReg * wrcmd=new RD53A::WrReg();
    	wrcmd->SetAddress(22);
    	wrcmd->SetValue(0xFFFF);
    	//client->SendCommand(0,wrcmd);
    	client->SendCommands(0,{wrcmd,rdcmd});
      
    }
    printServerMsg(" Shutting down server");
}



void QuasarServer::initialize()
{
    LOG(Log::INF) << "Initializing Quasar server.";
    /*
    for(Device::DQuad *quad : Device::DRoot::getInstance()->quads()){
    	for(Device::DRD53A *rd53a : quad->rd53as()){
    		client->Connect(0, "pcatlidros02", 12345, 12360, rd53a->getElink());
    	}
    }
    */
    try{
      client->Connect(0, "pcatlidros02", 12360, 12350, 
                      0,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,
                      0);
    }catch(...){
      LOG(Log::ERR) << "Cannot connect to felix";
    }
}

void QuasarServer::shutdown()
{
	LOG(Log::INF) << "Shutting down Quasar server.";
	client->Disconnect(0);
}

void QuasarServer::initializeLogIt()
{
	BaseQuasarServer::initializeLogIt();
    LOG(Log::INF) << "Logging initialized.";
}
