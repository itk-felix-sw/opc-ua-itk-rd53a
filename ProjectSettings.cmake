## ----------------------------------------------------
## Please see Documentation/quasarBuildSystem.html for
## information how to use this file.
## ----------------------------------------------------

include_directories( $ENV{XERCESC_ROOT}/include )

set(CUSTOM_SERVER_MODULES)

set(EXECUTABLE opc-ua-itk-rd53a)

set(SERVER_INCLUDE_DIRECTORIES $ENV{TDAQ_INST_PATH}/include 
                               $ENV{ITK_INST_PATH}/include 
                               $ENV{ITK_DCS_INST_PATH}/include 
                              )

set(SERVER_LINK_LIBRARIES -lnetio -lRD53A -lRD53ANetio)

set(SERVER_LINK_DIRECTORIES $ENV{XERCESC_ROOT}/lib 
                            $ENV{TDAQ_INST_PATH}/$ENV{CMTCONFIG}/lib
                            $ENV{ITK_INST_PATH}/$ENV{CMTCONFIG}/lib )

##
## If ON, in addition to an executable, a shared object will be created.
##
set(BUILD_SERVER_SHARED_LIB OFF)

##
## Add here any additional boost libraries needed with their canonical name
## examples: date_time atomic etc.
## Note: boost paths are resolved either from $BOOST_ROOT if defined or system paths as fallback
##
set(ADDITIONAL_BOOST_LIBS )
